﻿using Dynastech.DirectoryServices;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ActiveDirectoryGUI
{
    public static class Global
    {
        static Global()
        {
            var configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .Build();

            var options = new DirectoryServiceOptions();
            configuration.GetSection(nameof(DirectoryServiceOptions)).Bind(options);

            var guiOptions = new ActiveDirectoryGUIOptions();
            configuration.GetSection(nameof(ActiveDirectoryGUIOptions)).Bind(guiOptions);
            Options = guiOptions;

            var services = new ServiceCollection();
            services.AddDirectoryServices(options);

            var provider = services.BuildServiceProvider();

            OrganizationService = provider.GetRequiredService<IDirectoryOrganizationService>();
            GroupService = provider.GetRequiredService<IDirectoryGroupService>();
            UserService = provider.GetRequiredService<IDirectoryUserService>();
        }

        public static readonly ActiveDirectoryGUIOptions Options;

        public static readonly IDirectoryOrganizationService OrganizationService;
        public static readonly IDirectoryGroupService GroupService;
        public static readonly IDirectoryUserService UserService;

        public const string LoadingText = "加载中..";
    }
}
