﻿using Dynastech.DirectoryServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ActiveDirectoryGUI
{
    public class ActiveDirectoryGUIOptions
    {
        public string RootOU { get; set; }
        public string UserKeyPropertyName { get; set; }
        public string OrganizationKeyPropertyName { get; set; }
        public string GroupKeyPropertyName { get; set; }
    }
}
