﻿using Dynastech.DirectoryServices;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ActiveDirectoryGUI
{
    public partial class FrmUserUpdate : Form
    {
        public FrmUserUpdate()
        {
            InitializeComponent();

            _options = Global.Options;
        }

        private readonly ActiveDirectoryGUIOptions _options;

        private static FrmUserUpdate _instance;
        public static FrmUserUpdate GetInstance
        {
            get
            {
                if (_instance == null || _instance.IsDisposed)
                {
                    _instance = new FrmUserUpdate();
                }

                return _instance;
            }
        }

        public void AutoFillAsync(string dn)
        {
            Task.Factory.StartNew(async () =>
            {
                await FillUserAsync(dn);
            });
        }

        private async Task FillUserAsync(string distinguishedName)
        {
            var originalText = Text;
            BeginInvoke((MethodInvoker)delegate
            {
                Text += $" - {Global.LoadingText}";
            });

            var user = await FindUserAsync(distinguishedName);

            BeginInvoke((MethodInvoker)delegate
            {
                Text = originalText;

                txtUserId.Text = user.ExtensionProperties[_options.UserKeyPropertyName];
                txtUserName.Text = user.Name;
                txtUserDisplayName.Text = user.DisplayName;
                txtUserSn.Text = user.Sn;
                txtUserGivenName.Text = user.GivenName;
                txtUserUserPrincipalName.Text = user.UserPrincipalName;
                txtUserSAMAccountName.Text = user.SAMAccountName;
                txtUserMail.Text = user.Mail;
                txtUserHomePhone.Text = user.HomePhone;
                txtUserMobile.Text = user.Mobile;
                txtUserTelephoneNumber.Text = user.TelephoneNumber;
                txtUserPager.Text = user.Pager;
                txtUserIsEnabled.Text = user.IsEnabled.ToString();
                txtUserCompany.Text = user.Company;
                txtUserHomeDrive.Text = user.ExtensionProperties["HomeDrive"];
                txtUserHomeDirectory.Text = user.ExtensionProperties["HomeDirectory"];
            });
        }


        private Task<DirectoryUser> FindUserAsync(string distinguishedName)
        {
            return Global.UserService.FindByDn(distinguishedName);
        }
    }
}
