﻿using Dynastech.DirectoryServices;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ActiveDirectoryGUI
{
    public partial class FrmManage : Form
    {
        public FrmManage()
        {
            InitializeComponent();

            _options = Global.Options;
        }

        private readonly ActiveDirectoryGUIOptions _options;

        private static FrmManage _instance;
        public static FrmManage GetInstance
        {
            get
            {
                if (_instance == null || _instance.IsDisposed)
                {
                    _instance = new FrmManage();
                }

                return _instance;
            }
        }

        private void FrmManage_Load(object sender, EventArgs e)
        {
            treeOU.NodeMouseDoubleClick += TreeOU_NodeMouseDoubleClick;
            treeOU.NodeMouseClick += TreeOU_NodeMouseClick;
            tabControl.Selecting += TabControl_Selecting;
            dataGridViewUser.CellClick += DataGridViewUser_CellClick;
            dataGridViewGroup.CellClick += DataGridViewGroup_CellClick;


            Task.Factory.StartNew(InitialsAsync);
        }

        private void DataGridViewGroup_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == dataGridViewGroup.Columns.Count - 1)
            {
                var dn = dataGridViewGroup.Rows[e.RowIndex].Cells[DirectoryKnownNames.distinguishedName].Value.ToString();
                var instance = FrmGroupUpdate.GetInstance;
                instance.TopLevel = false;
                instance.Parent = this;
                instance.Show();
                instance.BringToFront();
                instance.AutoFillAsync(dn);
            }
        }

        private void DataGridViewUser_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == dataGridViewUser.Columns.Count - 1)
            {
                var dn = dataGridViewUser.Rows[e.RowIndex].Cells[DirectoryKnownNames.distinguishedName].Value.ToString();
                var instance = FrmUserUpdate.GetInstance;
                instance.TopLevel = false;
                instance.Parent = this;
                instance.Show();
                instance.BringToFront();
                instance.AutoFillAsync(dn);
            }
        }

        private void TabControl_Selecting(object sender, TabControlCancelEventArgs e)
        {
            Task.Factory.StartNew(AutoFillAsync, e.TabPage.Tag);
        }

        private void TreeOU_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            Task.Factory.StartNew(async () =>
            {
                var node = (TreeNodeOrganization)e.Node;
                await FillOUAsync(node);

                tabControl.BeginInvoke((MethodInvoker)async delegate
                {
                    await AutoFillAsync(tabControl.SelectedTab.Tag);
                });
            });
        }

        private void TreeOU_NodeMouseDoubleClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            var node = (TreeNodeOrganization)e.Node;
            Task.Factory.StartNew(LoadChildrenAsync, node);
        }






        private async Task AutoFillAsync(object tabPageTag)
        {
            switch (tabPageTag.ToString())
            {
                case "ou":
                    await FillOUAsync((TreeNodeOrganization)treeOU.SelectedNode);
                    break;
                case "users":
                    await FillUsersAsync();
                    break;
                case "groups":
                    await FillGroupsAsync();
                    break;
                default:
                    throw new NotSupportedException($"不支持的类型 {tabPageTag}");
            }
        }
        private async Task LoadChildrenAsync(object obj)
        {
            var node = (TreeNodeOrganization)obj;
            var nodeOriginalValue = node.Text;

            treeOU.BeginInvoke((MethodInvoker)delegate
            {
                node.Nodes.Clear();
                node.Text = Global.LoadingText;
            });

            var subOrganizations = await GetSubOrganizationsAsync(node.DistinguishedName);
            var nodes = subOrganizations.Select(x
                    => new TreeNodeOrganization(
                        x.DistinguishedName,
                        x.Name));

            treeOU.BeginInvoke((MethodInvoker)delegate
            {
                node.AddChildren(nodes);
                node.Text = nodeOriginalValue;
                node.Expand();
            });
        }
        private async Task FillOUAsync(object obj)
        {
            var originalText = tpOU.Text;
            tpOU.BeginInvoke((MethodInvoker)delegate
            {
                tpOU.Text += $" - {Global.LoadingText}";
            });

            var node = (TreeNodeOrganization)obj;
            var organization = await FindOrganizationAsync(node.DistinguishedName);

            treeOU.BeginInvoke((MethodInvoker)delegate
            {
                tpOU.Text = originalText;

                txtOUId.Text = organization.ExtensionProperties[_options.OrganizationKeyPropertyName];
                txtOUName.Text = organization.Name;
                txtOUDisplayName.Text = organization.DisplayName;
                txtOUDescription.Text = organization.Description;
            });
        }
        private Task FillUsersAsync()
        {
            var originalText = tpUsers.Text;
            tpUsers.BeginInvoke((MethodInvoker)delegate
            {
                tpUsers.Text += $" - {Global.LoadingText}";
            });

            dataGridViewUser.BeginInvoke((MethodInvoker)async delegate
            {
                var node = (TreeNodeOrganization)treeOU.SelectedNode;
                var users = await GetSubUsersAsync(node.DistinguishedName);

                dataGridViewUser.Rows.Clear();
                foreach (var user in users)
                {
                    dataGridViewUser.Rows.Add(user.DistinguishedName, $"{user.Name}({user.SAMAccountName})", user.Mobile, user.Mail);
                }

                tpUsers.Text = originalText;
                dataGridViewUser.AutoResizeColumns();
            });

            return Task.CompletedTask;
        }
        private Task FillGroupsAsync()
        {
            var originalText = tpGroups.Text;
            tpGroups.BeginInvoke((MethodInvoker)delegate
            {
                tpGroups.Text += $" - {Global.LoadingText}";
            });

            dataGridViewGroup.BeginInvoke((MethodInvoker)async delegate
            {
                var node = (TreeNodeOrganization)treeOU.SelectedNode;
                var groups = await GetSubGroupsAsync(node.DistinguishedName);

                dataGridViewGroup.Rows.Clear();
                foreach (var group in groups)
                {
                    dataGridViewGroup.Rows.Add(group.DistinguishedName, group.Name, group.SAMAccountName, group.Description);
                }

                tpGroups.Text = originalText;
                dataGridViewGroup.AutoResizeColumns();
            });

            return Task.CompletedTask;
        }
        private async Task InitialsAsync()
        {
            treeOU.BeginInvoke((MethodInvoker)delegate
            {
                treeOU.Nodes.Add(new TreeNodeOrganization(string.Empty, Global.LoadingText));

                var userDetailsButton = new DataGridViewButtonColumn
                {
                    Name = "操作",
                    Text = "详情",
                    UseColumnTextForButtonValue = true
                };
                dataGridViewUser.Columns.Add(DirectoryKnownNames.distinguishedName, DirectoryKnownNames.distinguishedName);
                dataGridViewUser.Columns.Add("Name", "姓名");
                dataGridViewUser.Columns.Add("Mobile", "移动电话");
                dataGridViewUser.Columns.Add("Email", "邮箱地址");
                dataGridViewUser.Columns.Add(userDetailsButton);
                dataGridViewUser.Columns[DirectoryKnownNames.distinguishedName].Visible = false;

                var groupDetailsButton = new DataGridViewButtonColumn
                {
                    Name = "操作",
                    Text = "详情",
                    UseColumnTextForButtonValue = true
                };
                dataGridViewGroup.Columns.Add(DirectoryKnownNames.distinguishedName, DirectoryKnownNames.distinguishedName);
                dataGridViewGroup.Columns.Add("Name", "名称");
                dataGridViewGroup.Columns.Add("AccountName", "账号");
                dataGridViewGroup.Columns.Add("Description", "描述");
                dataGridViewGroup.Columns.Add(groupDetailsButton);
                dataGridViewGroup.Columns[DirectoryKnownNames.distinguishedName].Visible = false;
            });

            var root = await FindOrganizationAsync(_options.RootOU);
            var subOrganizations = await GetSubOrganizationsAsync(_options.RootOU);

            var rootNode = new TreeNodeOrganization(
                root.DistinguishedName,
                root.Name,
                subOrganizations.Select(x
                    => new TreeNodeOrganization(
                        x.DistinguishedName,
                        x.Name)))
            {
                Checked = true
            };

            treeOU.BeginInvoke((MethodInvoker)delegate
            {
                treeOU.Nodes.Clear();
                treeOU.Nodes.Add(rootNode);
                treeOU.SelectedNode = rootNode;
                treeOU.Nodes[0].Expand();
                treeOU.HideSelection = false;
            });
        }


        private Task<DirectoryOrganization> FindOrganizationAsync(string distinguishedName)
        {
            return Global.OrganizationService.FindByDn(distinguishedName);
        }
        private Task<IEnumerable<DirectoryOrganization>> GetSubOrganizationsAsync(string distinguishedName)
        {
            return Global.OrganizationService.GetSubOrganizationsByDn(distinguishedName, DirectorySearchScope.OneLevel);
        }
        private Task<IEnumerable<DirectoryUser>> GetSubUsersAsync(string distinguishedName)
        {
            return Global.OrganizationService.GetSubUsersByDn(distinguishedName, DirectorySearchScope.OneLevel);
        }
        private Task<IEnumerable<DirectoryGroup>> GetSubGroupsAsync(string distinguishedName)
        {
            return Global.OrganizationService.GetSubGroupsByDn(distinguishedName, DirectorySearchScope.OneLevel);
        }
    }
}
