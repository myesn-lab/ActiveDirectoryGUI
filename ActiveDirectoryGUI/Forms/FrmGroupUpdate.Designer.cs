﻿namespace ActiveDirectoryGUI
{
    partial class FrmGroupUpdate
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnGroupUpdate = new System.Windows.Forms.Button();
            this.txtGroupGroupType = new System.Windows.Forms.TextBox();
            this.txtGroupSAMAccountName = new System.Windows.Forms.TextBox();
            this.txtGroupDisplayName = new System.Windows.Forms.TextBox();
            this.txtGroupId = new System.Windows.Forms.TextBox();
            this.txtGroupName = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnGroupUpdate
            // 
            this.btnGroupUpdate.Location = new System.Drawing.Point(137, 264);
            this.btnGroupUpdate.Name = "btnGroupUpdate";
            this.btnGroupUpdate.Size = new System.Drawing.Size(245, 60);
            this.btnGroupUpdate.TabIndex = 22;
            this.btnGroupUpdate.Text = "更新";
            this.btnGroupUpdate.UseVisualStyleBackColor = true;
            this.btnGroupUpdate.Visible = false;
            // 
            // txtGroupGroupType
            // 
            this.txtGroupGroupType.Location = new System.Drawing.Point(137, 210);
            this.txtGroupGroupType.Name = "txtGroupGroupType";
            this.txtGroupGroupType.Size = new System.Drawing.Size(245, 21);
            this.txtGroupGroupType.TabIndex = 17;
            // 
            // txtGroupSAMAccountName
            // 
            this.txtGroupSAMAccountName.Location = new System.Drawing.Point(137, 168);
            this.txtGroupSAMAccountName.Name = "txtGroupSAMAccountName";
            this.txtGroupSAMAccountName.Size = new System.Drawing.Size(245, 21);
            this.txtGroupSAMAccountName.TabIndex = 18;
            // 
            // txtGroupDisplayName
            // 
            this.txtGroupDisplayName.Location = new System.Drawing.Point(137, 125);
            this.txtGroupDisplayName.Name = "txtGroupDisplayName";
            this.txtGroupDisplayName.Size = new System.Drawing.Size(245, 21);
            this.txtGroupDisplayName.TabIndex = 19;
            // 
            // txtGroupId
            // 
            this.txtGroupId.Location = new System.Drawing.Point(137, 39);
            this.txtGroupId.Name = "txtGroupId";
            this.txtGroupId.Size = new System.Drawing.Size(245, 21);
            this.txtGroupId.TabIndex = 20;
            // 
            // txtGroupName
            // 
            this.txtGroupName.Location = new System.Drawing.Point(137, 82);
            this.txtGroupName.Name = "txtGroupName";
            this.txtGroupName.Size = new System.Drawing.Size(245, 21);
            this.txtGroupName.TabIndex = 21;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(63, 215);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(71, 12);
            this.label24.TabIndex = 12;
            this.label24.Text = "GroupType：";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(33, 173);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(101, 12);
            this.label17.TabIndex = 13;
            this.label17.Text = "SAMAccountName：";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(105, 45);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(29, 12);
            this.label21.TabIndex = 14;
            this.label21.Text = "Id：";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(51, 131);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(83, 12);
            this.label22.TabIndex = 15;
            this.label22.Text = "DisplayName：";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(93, 88);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(41, 12);
            this.label23.TabIndex = 16;
            this.label23.Text = "Name：";
            // 
            // FrmGroupUpdate
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(480, 364);
            this.Controls.Add(this.btnGroupUpdate);
            this.Controls.Add(this.txtGroupGroupType);
            this.Controls.Add(this.txtGroupSAMAccountName);
            this.Controls.Add(this.txtGroupDisplayName);
            this.Controls.Add(this.txtGroupId);
            this.Controls.Add(this.txtGroupName);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.label23);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "FrmGroupUpdate";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "通讯组详情";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnGroupUpdate;
        private System.Windows.Forms.TextBox txtGroupGroupType;
        private System.Windows.Forms.TextBox txtGroupSAMAccountName;
        private System.Windows.Forms.TextBox txtGroupDisplayName;
        private System.Windows.Forms.TextBox txtGroupId;
        private System.Windows.Forms.TextBox txtGroupName;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
    }
}