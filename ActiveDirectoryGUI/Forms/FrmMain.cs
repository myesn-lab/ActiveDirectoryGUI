﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ActiveDirectoryGUI
{
    public partial class FrmMain : Form
    {
        public FrmMain()
        {
            InitializeComponent();
        }

        private void FrmMain_Load(object sender, EventArgs e)
        {
            tsmiADManage.Click += TsmiADManage_Click;
            tsmiLanguageSwitch.Click += TsmiLanguageSwitch_Click;

            IsMdiContainer = true;
        }

        private bool _iszh = true;
        private void TsmiLanguageSwitch_Click(object sender, EventArgs e)
        {
            _iszh = !_iszh;

            if (_iszh)
            {
                LanguageHelper.SetLang("zh", this, typeof(FrmMain));
                LanguageHelper.SetLang("zh", this, typeof(FrmManage));
            }
            else
            {
                LanguageHelper.SetLang("en", this, typeof(FrmMain));
                LanguageHelper.SetLang("en", this, typeof(FrmManage));
            }
        }

        private void TsmiADManage_Click(object sender, EventArgs e)
        {
            var instance = FrmManage.GetInstance;
            instance.TopLevel = false;
            //instance.Parent = this;
            instance.MdiParent = this;
            instance.Show();
            instance.BringToFront();
        }
    }
}
