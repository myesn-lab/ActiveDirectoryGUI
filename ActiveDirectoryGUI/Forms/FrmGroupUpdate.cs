﻿using Dynastech.DirectoryServices;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ActiveDirectoryGUI
{
    public partial class FrmGroupUpdate : Form
    {
        public FrmGroupUpdate()
        {
            InitializeComponent();

            _options = Global.Options;
        }

        private readonly ActiveDirectoryGUIOptions _options;

        private static FrmGroupUpdate _instance;
        public static FrmGroupUpdate GetInstance
        {
            get
            {
                if (_instance == null || _instance.IsDisposed)
                {
                    _instance = new FrmGroupUpdate();
                }

                return _instance;
            }
        }

        public void AutoFillAsync(string dn)
        {
            Task.Factory.StartNew(async () =>
            {
                await FillGroupAsync(dn);
            });
        }

        private async Task FillGroupAsync(string distinguishedName)
        {
            var originalText = Text;
            BeginInvoke((MethodInvoker)delegate
            {
                Text += $" - {Global.LoadingText}";
            });

            var group = await FindGroupAsync(distinguishedName);

            BeginInvoke((MethodInvoker)delegate
            {
                Text = originalText;

                txtGroupId.Text = group.ExtensionProperties[_options.GroupKeyPropertyName];
                txtGroupName.Text = group.Name;
                txtGroupDisplayName.Text = group.DisplayName;
                txtGroupSAMAccountName.Text = group.SAMAccountName;
                txtGroupGroupType.Text = group.GroupType.ToString();
            });
        }


        private Task<DirectoryGroup> FindGroupAsync(string distinguishedName)
        {
            return Global.GroupService.FindByDn(distinguishedName);
        }
    }
}
