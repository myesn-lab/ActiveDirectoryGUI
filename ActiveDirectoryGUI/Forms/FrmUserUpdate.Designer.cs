﻿namespace ActiveDirectoryGUI
{
    partial class FrmUserUpdate
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnUserUpdate = new System.Windows.Forms.Button();
            this.txtUserHomeDirectory = new System.Windows.Forms.TextBox();
            this.txtUserMail = new System.Windows.Forms.TextBox();
            this.txtUserPager = new System.Windows.Forms.TextBox();
            this.txtUserSn = new System.Windows.Forms.TextBox();
            this.txtUserCompany = new System.Windows.Forms.TextBox();
            this.txtUserUserPrincipalName = new System.Windows.Forms.TextBox();
            this.txtUserMobile = new System.Windows.Forms.TextBox();
            this.txtUserName = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.txtUserHomeDrive = new System.Windows.Forms.TextBox();
            this.txtUserSAMAccountName = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.txtUserTelephoneNumber = new System.Windows.Forms.TextBox();
            this.txtUserDisplayName = new System.Windows.Forms.TextBox();
            this.lblss = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtUserIsEnabled = new System.Windows.Forms.TextBox();
            this.txtUserGivenName = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtUserHomePhone = new System.Windows.Forms.TextBox();
            this.txtUserId = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnUserUpdate
            // 
            this.btnUserUpdate.Location = new System.Drawing.Point(289, 358);
            this.btnUserUpdate.Name = "btnUserUpdate";
            this.btnUserUpdate.Size = new System.Drawing.Size(245, 60);
            this.btnUserUpdate.TabIndex = 35;
            this.btnUserUpdate.Text = "更新";
            this.btnUserUpdate.UseVisualStyleBackColor = true;
            this.btnUserUpdate.Visible = false;
            // 
            // txtUserHomeDirectory
            // 
            this.txtUserHomeDirectory.Location = new System.Drawing.Point(520, 304);
            this.txtUserHomeDirectory.Name = "txtUserHomeDirectory";
            this.txtUserHomeDirectory.Size = new System.Drawing.Size(189, 21);
            this.txtUserHomeDirectory.TabIndex = 22;
            // 
            // txtUserMail
            // 
            this.txtUserMail.Location = new System.Drawing.Point(149, 306);
            this.txtUserMail.Name = "txtUserMail";
            this.txtUserMail.Size = new System.Drawing.Size(189, 21);
            this.txtUserMail.TabIndex = 23;
            // 
            // txtUserPager
            // 
            this.txtUserPager.Location = new System.Drawing.Point(520, 140);
            this.txtUserPager.Name = "txtUserPager";
            this.txtUserPager.Size = new System.Drawing.Size(189, 21);
            this.txtUserPager.TabIndex = 24;
            // 
            // txtUserSn
            // 
            this.txtUserSn.Location = new System.Drawing.Point(149, 142);
            this.txtUserSn.Name = "txtUserSn";
            this.txtUserSn.Size = new System.Drawing.Size(189, 21);
            this.txtUserSn.TabIndex = 25;
            // 
            // txtUserCompany
            // 
            this.txtUserCompany.Location = new System.Drawing.Point(520, 218);
            this.txtUserCompany.Name = "txtUserCompany";
            this.txtUserCompany.Size = new System.Drawing.Size(189, 21);
            this.txtUserCompany.TabIndex = 28;
            // 
            // txtUserUserPrincipalName
            // 
            this.txtUserUserPrincipalName.Location = new System.Drawing.Point(149, 220);
            this.txtUserUserPrincipalName.Name = "txtUserUserPrincipalName";
            this.txtUserUserPrincipalName.Size = new System.Drawing.Size(189, 21);
            this.txtUserUserPrincipalName.TabIndex = 29;
            // 
            // txtUserMobile
            // 
            this.txtUserMobile.Location = new System.Drawing.Point(520, 54);
            this.txtUserMobile.Name = "txtUserMobile";
            this.txtUserMobile.Size = new System.Drawing.Size(189, 21);
            this.txtUserMobile.TabIndex = 30;
            // 
            // txtUserName
            // 
            this.txtUserName.Location = new System.Drawing.Point(149, 56);
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new System.Drawing.Size(189, 21);
            this.txtUserName.TabIndex = 31;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(422, 308);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(95, 12);
            this.label20.TabIndex = 18;
            this.label20.Text = "HomeDirectory：";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(105, 310);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(41, 12);
            this.label12.TabIndex = 17;
            this.label12.Text = "Mail：";
            // 
            // txtUserHomeDrive
            // 
            this.txtUserHomeDrive.Location = new System.Drawing.Point(520, 264);
            this.txtUserHomeDrive.Name = "txtUserHomeDrive";
            this.txtUserHomeDrive.Size = new System.Drawing.Size(189, 21);
            this.txtUserHomeDrive.TabIndex = 19;
            // 
            // txtUserSAMAccountName
            // 
            this.txtUserSAMAccountName.Location = new System.Drawing.Point(149, 266);
            this.txtUserSAMAccountName.Name = "txtUserSAMAccountName";
            this.txtUserSAMAccountName.Size = new System.Drawing.Size(189, 21);
            this.txtUserSAMAccountName.TabIndex = 34;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(470, 144);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(47, 12);
            this.label19.TabIndex = 15;
            this.label19.Text = "Pager：";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(117, 146);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(29, 12);
            this.label8.TabIndex = 14;
            this.label8.Text = "Sn：";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(458, 222);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(59, 12);
            this.label18.TabIndex = 13;
            this.label18.Text = "Company：";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(27, 224);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(119, 12);
            this.label11.TabIndex = 3;
            this.label11.Text = "UserPrincipalName：";
            // 
            // txtUserTelephoneNumber
            // 
            this.txtUserTelephoneNumber.Location = new System.Drawing.Point(520, 100);
            this.txtUserTelephoneNumber.Name = "txtUserTelephoneNumber";
            this.txtUserTelephoneNumber.Size = new System.Drawing.Size(189, 21);
            this.txtUserTelephoneNumber.TabIndex = 33;
            // 
            // txtUserDisplayName
            // 
            this.txtUserDisplayName.Location = new System.Drawing.Point(149, 102);
            this.txtUserDisplayName.Name = "txtUserDisplayName";
            this.txtUserDisplayName.Size = new System.Drawing.Size(189, 21);
            this.txtUserDisplayName.TabIndex = 32;
            // 
            // lblss
            // 
            this.lblss.AutoSize = true;
            this.lblss.Location = new System.Drawing.Point(446, 268);
            this.lblss.Name = "lblss";
            this.lblss.Size = new System.Drawing.Size(71, 12);
            this.lblss.TabIndex = 11;
            this.lblss.Text = "HomeDrive：";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(45, 270);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(101, 12);
            this.label10.TabIndex = 10;
            this.label10.Text = "SAMAccountName：";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(464, 58);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(53, 12);
            this.label16.TabIndex = 9;
            this.label16.Text = "Mobile：";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(105, 60);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 12);
            this.label6.TabIndex = 8;
            this.label6.Text = "Name：";
            // 
            // txtUserIsEnabled
            // 
            this.txtUserIsEnabled.Location = new System.Drawing.Point(520, 178);
            this.txtUserIsEnabled.Name = "txtUserIsEnabled";
            this.txtUserIsEnabled.Size = new System.Drawing.Size(189, 21);
            this.txtUserIsEnabled.TabIndex = 27;
            // 
            // txtUserGivenName
            // 
            this.txtUserGivenName.Location = new System.Drawing.Point(149, 180);
            this.txtUserGivenName.Name = "txtUserGivenName";
            this.txtUserGivenName.Size = new System.Drawing.Size(189, 21);
            this.txtUserGivenName.TabIndex = 26;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(410, 106);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(107, 12);
            this.label15.TabIndex = 7;
            this.label15.Text = "TelephoneNumber：";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(63, 106);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(83, 12);
            this.label7.TabIndex = 6;
            this.label7.Text = "DisplayName：";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(446, 182);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(71, 12);
            this.label14.TabIndex = 5;
            this.label14.Text = "IsEnabled：";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(75, 184);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(71, 12);
            this.label9.TabIndex = 4;
            this.label9.Text = "GivenName：";
            // 
            // txtUserHomePhone
            // 
            this.txtUserHomePhone.Location = new System.Drawing.Point(520, 14);
            this.txtUserHomePhone.Name = "txtUserHomePhone";
            this.txtUserHomePhone.Size = new System.Drawing.Size(189, 21);
            this.txtUserHomePhone.TabIndex = 21;
            // 
            // txtUserId
            // 
            this.txtUserId.Location = new System.Drawing.Point(149, 16);
            this.txtUserId.Name = "txtUserId";
            this.txtUserId.Size = new System.Drawing.Size(189, 21);
            this.txtUserId.TabIndex = 20;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(446, 18);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(71, 12);
            this.label13.TabIndex = 12;
            this.label13.Text = "HomePhone：";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(117, 20);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(29, 12);
            this.label5.TabIndex = 16;
            this.label5.Text = "Id：";
            // 
            // FrmUserUpdate
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btnUserUpdate);
            this.Controls.Add(this.txtUserHomeDirectory);
            this.Controls.Add(this.txtUserMail);
            this.Controls.Add(this.txtUserPager);
            this.Controls.Add(this.txtUserSn);
            this.Controls.Add(this.txtUserCompany);
            this.Controls.Add(this.txtUserUserPrincipalName);
            this.Controls.Add(this.txtUserMobile);
            this.Controls.Add(this.txtUserName);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.txtUserHomeDrive);
            this.Controls.Add(this.txtUserSAMAccountName);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.txtUserTelephoneNumber);
            this.Controls.Add(this.txtUserDisplayName);
            this.Controls.Add(this.lblss);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtUserIsEnabled);
            this.Controls.Add(this.txtUserGivenName);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txtUserHomePhone);
            this.Controls.Add(this.txtUserId);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label5);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "FrmUserUpdate";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "用户详情";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnUserUpdate;
        private System.Windows.Forms.TextBox txtUserHomeDirectory;
        private System.Windows.Forms.TextBox txtUserMail;
        private System.Windows.Forms.TextBox txtUserPager;
        private System.Windows.Forms.TextBox txtUserSn;
        private System.Windows.Forms.TextBox txtUserCompany;
        private System.Windows.Forms.TextBox txtUserUserPrincipalName;
        private System.Windows.Forms.TextBox txtUserMobile;
        private System.Windows.Forms.TextBox txtUserName;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtUserHomeDrive;
        private System.Windows.Forms.TextBox txtUserSAMAccountName;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtUserTelephoneNumber;
        private System.Windows.Forms.TextBox txtUserDisplayName;
        private System.Windows.Forms.Label lblss;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtUserIsEnabled;
        private System.Windows.Forms.TextBox txtUserGivenName;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtUserHomePhone;
        private System.Windows.Forms.TextBox txtUserId;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label5;
    }
}