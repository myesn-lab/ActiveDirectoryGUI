﻿namespace ActiveDirectoryGUI
{
    partial class FrmManage
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.sc = new System.Windows.Forms.SplitContainer();
            this.treeOU = new System.Windows.Forms.TreeView();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tpOU = new System.Windows.Forms.TabPage();
            this.btnOUUpdate = new System.Windows.Forms.Button();
            this.txtOUDescription = new System.Windows.Forms.TextBox();
            this.txtOUDisplayName = new System.Windows.Forms.TextBox();
            this.txtOUId = new System.Windows.Forms.TextBox();
            this.txtOUName = new System.Windows.Forms.TextBox();
            this.lblOUDescription = new System.Windows.Forms.Label();
            this.lblOUId = new System.Windows.Forms.Label();
            this.lblOUDisplayName = new System.Windows.Forms.Label();
            this.lblOUName = new System.Windows.Forms.Label();
            this.tpUsers = new System.Windows.Forms.TabPage();
            this.dataGridViewUser = new System.Windows.Forms.DataGridView();
            this.tpGroups = new System.Windows.Forms.TabPage();
            this.dataGridViewGroup = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.sc)).BeginInit();
            this.sc.Panel1.SuspendLayout();
            this.sc.Panel2.SuspendLayout();
            this.sc.SuspendLayout();
            this.tabControl.SuspendLayout();
            this.tpOU.SuspendLayout();
            this.tpUsers.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewUser)).BeginInit();
            this.tpGroups.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewGroup)).BeginInit();
            this.SuspendLayout();
            // 
            // sc
            // 
            this.sc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sc.Location = new System.Drawing.Point(0, 0);
            this.sc.Name = "sc";
            // 
            // sc.Panel1
            // 
            this.sc.Panel1.Controls.Add(this.treeOU);
            // 
            // sc.Panel2
            // 
            this.sc.Panel2.Controls.Add(this.tabControl);
            this.sc.Size = new System.Drawing.Size(663, 408);
            this.sc.SplitterDistance = 232;
            this.sc.TabIndex = 0;
            // 
            // treeOU
            // 
            this.treeOU.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeOU.Location = new System.Drawing.Point(0, 0);
            this.treeOU.Name = "treeOU";
            this.treeOU.Size = new System.Drawing.Size(232, 408);
            this.treeOU.TabIndex = 0;
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.tpOU);
            this.tabControl.Controls.Add(this.tpUsers);
            this.tabControl.Controls.Add(this.tpGroups);
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.Location = new System.Drawing.Point(0, 0);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(427, 408);
            this.tabControl.TabIndex = 0;
            // 
            // tpOU
            // 
            this.tpOU.Controls.Add(this.btnOUUpdate);
            this.tpOU.Controls.Add(this.txtOUDescription);
            this.tpOU.Controls.Add(this.txtOUDisplayName);
            this.tpOU.Controls.Add(this.txtOUId);
            this.tpOU.Controls.Add(this.txtOUName);
            this.tpOU.Controls.Add(this.lblOUDescription);
            this.tpOU.Controls.Add(this.lblOUId);
            this.tpOU.Controls.Add(this.lblOUDisplayName);
            this.tpOU.Controls.Add(this.lblOUName);
            this.tpOU.Location = new System.Drawing.Point(4, 22);
            this.tpOU.Name = "tpOU";
            this.tpOU.Padding = new System.Windows.Forms.Padding(3);
            this.tpOU.Size = new System.Drawing.Size(419, 382);
            this.tpOU.TabIndex = 0;
            this.tpOU.Tag = "ou";
            this.tpOU.Text = "组织详情";
            this.tpOU.UseVisualStyleBackColor = true;
            // 
            // btnOUUpdate
            // 
            this.btnOUUpdate.Location = new System.Drawing.Point(127, 310);
            this.btnOUUpdate.Name = "btnOUUpdate";
            this.btnOUUpdate.Size = new System.Drawing.Size(245, 60);
            this.btnOUUpdate.TabIndex = 2;
            this.btnOUUpdate.Text = "更新";
            this.btnOUUpdate.UseVisualStyleBackColor = true;
            this.btnOUUpdate.Visible = false;
            // 
            // txtOUDescription
            // 
            this.txtOUDescription.Location = new System.Drawing.Point(127, 215);
            this.txtOUDescription.Multiline = true;
            this.txtOUDescription.Name = "txtOUDescription";
            this.txtOUDescription.Size = new System.Drawing.Size(245, 73);
            this.txtOUDescription.TabIndex = 1;
            // 
            // txtOUDisplayName
            // 
            this.txtOUDisplayName.Location = new System.Drawing.Point(127, 172);
            this.txtOUDisplayName.Name = "txtOUDisplayName";
            this.txtOUDisplayName.Size = new System.Drawing.Size(245, 21);
            this.txtOUDisplayName.TabIndex = 1;
            // 
            // txtOUId
            // 
            this.txtOUId.Location = new System.Drawing.Point(127, 86);
            this.txtOUId.Name = "txtOUId";
            this.txtOUId.Size = new System.Drawing.Size(245, 21);
            this.txtOUId.TabIndex = 1;
            // 
            // txtOUName
            // 
            this.txtOUName.Location = new System.Drawing.Point(127, 129);
            this.txtOUName.Name = "txtOUName";
            this.txtOUName.Size = new System.Drawing.Size(245, 21);
            this.txtOUName.TabIndex = 1;
            // 
            // lblOUDescription
            // 
            this.lblOUDescription.AutoSize = true;
            this.lblOUDescription.Location = new System.Drawing.Point(41, 219);
            this.lblOUDescription.Name = "lblOUDescription";
            this.lblOUDescription.Size = new System.Drawing.Size(83, 12);
            this.lblOUDescription.TabIndex = 0;
            this.lblOUDescription.Text = "Description：";
            // 
            // lblOUId
            // 
            this.lblOUId.AutoSize = true;
            this.lblOUId.Location = new System.Drawing.Point(95, 91);
            this.lblOUId.Name = "lblOUId";
            this.lblOUId.Size = new System.Drawing.Size(29, 12);
            this.lblOUId.TabIndex = 0;
            this.lblOUId.Text = "Id：";
            // 
            // lblOUDisplayName
            // 
            this.lblOUDisplayName.AutoSize = true;
            this.lblOUDisplayName.Location = new System.Drawing.Point(41, 177);
            this.lblOUDisplayName.Name = "lblOUDisplayName";
            this.lblOUDisplayName.Size = new System.Drawing.Size(83, 12);
            this.lblOUDisplayName.TabIndex = 0;
            this.lblOUDisplayName.Text = "DisplayName：";
            // 
            // lblOUName
            // 
            this.lblOUName.AutoSize = true;
            this.lblOUName.Location = new System.Drawing.Point(83, 134);
            this.lblOUName.Name = "lblOUName";
            this.lblOUName.Size = new System.Drawing.Size(41, 12);
            this.lblOUName.TabIndex = 0;
            this.lblOUName.Text = "Name：";
            // 
            // tpUsers
            // 
            this.tpUsers.Controls.Add(this.dataGridViewUser);
            this.tpUsers.Location = new System.Drawing.Point(4, 22);
            this.tpUsers.Name = "tpUsers";
            this.tpUsers.Padding = new System.Windows.Forms.Padding(3);
            this.tpUsers.Size = new System.Drawing.Size(419, 382);
            this.tpUsers.TabIndex = 1;
            this.tpUsers.Tag = "users";
            this.tpUsers.Text = "用户列表";
            this.tpUsers.UseVisualStyleBackColor = true;
            // 
            // dataGridViewUser
            // 
            this.dataGridViewUser.AllowUserToAddRows = false;
            this.dataGridViewUser.AllowUserToDeleteRows = false;
            this.dataGridViewUser.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewUser.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewUser.Location = new System.Drawing.Point(3, 3);
            this.dataGridViewUser.Name = "dataGridViewUser";
            this.dataGridViewUser.ReadOnly = true;
            this.dataGridViewUser.RowTemplate.Height = 23;
            this.dataGridViewUser.Size = new System.Drawing.Size(413, 376);
            this.dataGridViewUser.TabIndex = 0;
            // 
            // tpGroups
            // 
            this.tpGroups.Controls.Add(this.dataGridViewGroup);
            this.tpGroups.Location = new System.Drawing.Point(4, 22);
            this.tpGroups.Name = "tpGroups";
            this.tpGroups.Padding = new System.Windows.Forms.Padding(3);
            this.tpGroups.Size = new System.Drawing.Size(419, 382);
            this.tpGroups.TabIndex = 2;
            this.tpGroups.Tag = "groups";
            this.tpGroups.Text = "通讯组列表";
            this.tpGroups.UseVisualStyleBackColor = true;
            // 
            // dataGridViewGroup
            // 
            this.dataGridViewGroup.AllowUserToAddRows = false;
            this.dataGridViewGroup.AllowUserToDeleteRows = false;
            this.dataGridViewGroup.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewGroup.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewGroup.Location = new System.Drawing.Point(3, 3);
            this.dataGridViewGroup.Name = "dataGridViewGroup";
            this.dataGridViewGroup.ReadOnly = true;
            this.dataGridViewGroup.RowTemplate.Height = 23;
            this.dataGridViewGroup.Size = new System.Drawing.Size(413, 376);
            this.dataGridViewGroup.TabIndex = 0;
            // 
            // FrmManage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(663, 408);
            this.Controls.Add(this.sc);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "FrmManage";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "域管理工具";
            this.Load += new System.EventHandler(this.FrmManage_Load);
            this.sc.Panel1.ResumeLayout(false);
            this.sc.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.sc)).EndInit();
            this.sc.ResumeLayout(false);
            this.tabControl.ResumeLayout(false);
            this.tpOU.ResumeLayout(false);
            this.tpOU.PerformLayout();
            this.tpUsers.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewUser)).EndInit();
            this.tpGroups.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewGroup)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer sc;
        private System.Windows.Forms.TreeView treeOU;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tpOU;
        private System.Windows.Forms.TabPage tpUsers;
        private System.Windows.Forms.TabPage tpGroups;
        private System.Windows.Forms.TextBox txtOUDescription;
        private System.Windows.Forms.TextBox txtOUDisplayName;
        private System.Windows.Forms.TextBox txtOUName;
        private System.Windows.Forms.Label lblOUDescription;
        private System.Windows.Forms.Label lblOUDisplayName;
        private System.Windows.Forms.Label lblOUName;
        private System.Windows.Forms.TextBox txtOUId;
        private System.Windows.Forms.Label lblOUId;
        private System.Windows.Forms.Button btnOUUpdate;
        private System.Windows.Forms.DataGridView dataGridViewUser;
        private System.Windows.Forms.DataGridView dataGridViewGroup;
    }
}

