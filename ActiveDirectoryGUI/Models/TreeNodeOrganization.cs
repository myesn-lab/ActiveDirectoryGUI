﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ActiveDirectoryGUI
{
    public class TreeNodeOrganization : TreeNode
    {
        public TreeNodeOrganization(string distinguishedName, string text, IEnumerable<TreeNodeOrganization> children)
        {
            DistinguishedName = distinguishedName;
            Text = text;
            Nodes.AddRange(children.ToArray());
        }

        public TreeNodeOrganization(string distinguishedName, string text)
        {
            DistinguishedName = distinguishedName;
            Text = text;
        }

        public string DistinguishedName { get; set; }

        public void AddChildren(IEnumerable<TreeNodeOrganization> children)
        {
            Nodes.AddRange(children.Cast<TreeNode>().ToArray());
        }

    }
}
